from geo.http_client import GeoServiceClient
from metasearch.http import Server
from metasearch.service import MetaSearchService
from search.service import SearchInShardsService
from search_shard.http_client import SimpleSearchServiceClient
from settings import SHARD_NAMES
from user.http_client import UserServiceClient


def main():
    user_service = UserServiceClient()
    geo_service = GeoServiceClient()
    search = SearchInShardsService(shards=[SimpleSearchServiceClient(shard_address) for shard_address in SHARD_NAMES])
    metasearch = MetaSearchService(search, user_service, geo_service)
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
