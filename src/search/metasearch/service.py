from typing import List, Dict

from pathos.multiprocessing import Pool

from common.base_geo_service import BaseGeoService
from common.base_search_service import BaseSearchService
from common.base_user_service import BaseUserService


class MetaSearchService:
    def __init__(self, search: BaseSearchService, user_service: BaseUserService, geo_service: BaseGeoService):
        self._search = search
        self._user_service = user_service
        self._geo_service = geo_service
        self._pool = Pool()

    def __del__(self):
        self._pool.close()

    def search(self, search_text, user_id, ip, limit=10) -> List[Dict]:
        user_data_future = self._pool.apply_async(self._user_service.get_user_data,
                                                  (user_id,))  # {'gender': ..., 'age': ...}
        geo_data_future = self._pool.apply_async(self._geo_service.get_geo_data,
                                                 (ip,))  # {'region': ...}
        user_data = user_data_future.get()
        geo_data = geo_data_future.get()
        df = self._search.get_search_data(search_text, user_data, geo_data, limit)
        return df[self._search.DOCS_COLUMNS].to_dict('records')
