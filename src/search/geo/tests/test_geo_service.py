import pandas as pd
import pytest

from common.data_source import AbstractDataSource
from common.flask_wrappers import FlaskClientWrapper
from geo.http_client import GeoServiceClient
from geo.http_server import Server
from geo.service import GeoService


@pytest.fixture
def data_source():
    class SomeDataSource(AbstractDataSource):
        def read_data(self, *args, **kwargs):
            return pd.DataFrame([{'network': '192.168.1.0/24', 'country_name': 'Some Country'}])

    return SomeDataSource()


@pytest.fixture
def geo_service_impl(data_source):
    return GeoService(data_source)


@pytest.fixture
def geo_service_client(data_source):
    geo_service = GeoService(data_source)
    server = Server('geo', geo_service=geo_service)
    client = FlaskClientWrapper(server.test_client())
    client.application = server
    return GeoServiceClient(server_address="/", client=client)


def test_geo_service(geo_service_impl):
    assert geo_service_impl.get_geo_data('192.168.1.255') == {'region': 'Some Country'}
    assert geo_service_impl.get_geo_data('192.168.0.1') is None
    assert geo_service_impl.get_geo_data('abc') is None


def test_geo_service_client(geo_service_client):
    assert geo_service_client.get_geo_data('192.168.1.255') == {'region': 'Some Country'}
    assert geo_service_client.get_geo_data('192.168.0.1') is None
    assert geo_service_client.get_geo_data('abc') is None

# TODO: remove duplication, parametrise tests
