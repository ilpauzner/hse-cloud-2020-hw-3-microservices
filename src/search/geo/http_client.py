import urllib.parse

import requests

from common.base_geo_service import BaseGeoService


class GeoServiceClient(BaseGeoService):
    def __init__(self, server_address="http://geo_service:8000", client=requests):
        self.url = urllib.parse.urljoin(server_address, 'geo_data')
        self.client = client

    def get_geo_data(self, ip_addr):
        response = self.client.get(self.url, params={'ip_addr': ip_addr})
        return response.json()['geo_data']
