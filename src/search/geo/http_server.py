from flask import Flask, request

from geo.service import GeoService


class Server(Flask):
    def __init__(self, name: str, geo_service: GeoService):
        super().__init__(name)
        self._geo_service = geo_service
        urls = [
            ('/geo_data', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        ip_addr = request.args.get('ip_addr')
        geo_data = self._geo_service.get_geo_data(ip_addr)
        return {'geo_data': geo_data}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
