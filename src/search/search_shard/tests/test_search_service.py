import numpy as np
import pandas as pd
import pytest

from common.data_source import AbstractDataSource
from common.flask_wrappers import FlaskClientWrapper
from search_shard.http_client import SimpleSearchServiceClient
from search_shard.http_server import Server
from search_shard.service import SimpleSearchService


@pytest.fixture
def data_source():
    class SomeDataSource(AbstractDataSource):
        def read_data(self):
            return [{'document': 'some document', 'key': 'some_key', 'gender': 'male,female',
                     'age_from': 0, 'age_to': 10, 'region': 'Some Region'},
                    {'document': 'another document', 'key': 'another_key', 'gender': 'female',
                     'age_from': 15, 'age_to': 30, 'region': 'Another Region'},
                    {'document': 'some another document', 'key': 'some_another_key', 'gender': 'male',
                     'age_from': 20, 'age_to': 25, 'region': 'Some Region'}]

    return SomeDataSource()


@pytest.fixture
def search_service(data_source):
    return SimpleSearchService(data_source)


@pytest.fixture
def search_service_client(data_source):
    search_service = SimpleSearchService(data_source)
    server = Server('search_shard', search_service=search_service)
    client = FlaskClientWrapper(server.test_client())
    client.application = server
    return SimpleSearchServiceClient(server_address="/", client=client)


def test_build_tokens_count(search_service):
    res = search_service._build_tokens_count('some another')
    assert all(res == pd.Series([1, 1, 2]))


def test_get_geo_mask(search_service):
    res = search_service._get_geo_mask(geo_data={'region': 'Some Region'})
    assert all(res == pd.Series([True, False, True]))


def test_get_gender_mask(search_service):
    res = search_service._get_gender_mask(user_data={'gender': 'female'})
    assert all(res == pd.Series([True, True, False]))


def test_get_age_mask(search_service):
    res = search_service._get_age_mask(user_data={'age': '21'})
    assert all(res == pd.Series([False, True, True]))


def test_get_search_data(search_service):
    res = search_service.get_search_data(
        'some another',
        user_data={'gender': 'female', 'age': '21'},
        geo_data={'region': 'Some Region'},
        limit=2
    )
    expected = pd.Series(['some_another_key', 'some_key'])
    assert np.array_equal(res.key, expected)


def test_get_search_data_client(search_service_client):
    res = search_service_client.get_search_data(
        'some another',
        user_data={'gender': 'female', 'age': '21'},
        geo_data={'region': 'Some Region'},
        limit=2
    )
    expected = pd.Series(['some_another_key', 'some_key'])
    assert np.array_equal(res.key, expected)
