import json
import urllib.parse

import pandas as pd
import requests

from common.base_search_service import BaseSearchService


class SimpleSearchServiceClient(BaseSearchService):
    def __init__(self, server_address="http://search_shard:8000", client=requests):
        self.url = urllib.parse.urljoin(server_address, 'search')
        self.client = client

    def get_search_data(self, search_text, user_data=None, geo_data=None, limit=10):
        response = self.client.get(self.url, params={'search_text': search_text, 'user_data': json.dumps(user_data),
                                                     'geo_data': json.dumps(geo_data), 'limit': limit})
        return pd.read_json(response.json()['search_results'])
