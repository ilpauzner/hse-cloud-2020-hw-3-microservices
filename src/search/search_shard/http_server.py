import json

from flask import Flask, request

from search_shard.service import SimpleSearchService


class Server(Flask):
    def __init__(self, name: str, search_service: SimpleSearchService):
        super().__init__(name)
        self._search_service = search_service
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        search_text = request.args.get('search_text')
        user_data = request.args.get('user_data')
        if user_data is not None:
            user_data = json.loads(user_data)
        geo_data = request.args.get('geo_data')
        if geo_data is not None:
            geo_data = json.loads(geo_data)
        limit = request.args.get('limit')
        if limit is None:
            limit = 10
        else:
            limit = int(limit)
        sr = self._search_service.get_search_data(search_text, user_data, geo_data, limit)
        return {'search_results': sr.to_json()}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
