import sys

from common.data_source import CSV
from search_shard.http_server import Server
from search_shard.service import SimpleSearchService


def main():
    if len(sys.argv) == 2:
        filename = sys.argv[1]
        search_service = SimpleSearchService(CSV(filename))
        server = Server('search_shard', search_service=search_service)
        server.run_server(debug=True)


if __name__ == '__main__':
    main()
