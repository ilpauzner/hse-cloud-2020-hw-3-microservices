import numpy as np
import pandas as pd
import pytest

from common.data_source import AbstractDataSource
from search.service import SearchInShardsService
from search_shard.service import SimpleSearchService


@pytest.fixture
def data_source():
    class SomeDataSource(AbstractDataSource):
        def read_data(self):
            return [{'document': 'some document', 'key': 'some_key', 'gender': 'male,female',
                     'age_from': 0, 'age_to': 10, 'region': 'Some Region'},
                    {'document': 'another document', 'key': 'another_key', 'gender': 'female',
                     'age_from': 15, 'age_to': 30, 'region': 'Another Region'},
                    {'document': 'some another document', 'key': 'some_another_key', 'gender': 'male',
                     'age_from': 20, 'age_to': 25, 'region': 'Some Region'}]

    return SomeDataSource()


@pytest.fixture
def search_service(data_source):
    return SimpleSearchService(data_source)


def test_search_in_shards():
    class SomeDataSource(AbstractDataSource):
        def __init__(self, rows):
            self._rows = rows

        def read_data(self):
            return self._rows

    ds1 = SomeDataSource([{'document': 'some document', 'key': 'some_key', 'gender': 'male,female',
                           'age_from': 0, 'age_to': 10, 'region': 'Some Region'}])
    ds2 = SomeDataSource([{'document': 'another document', 'key': 'another_key', 'gender': 'female',
                           'age_from': 15, 'age_to': 30, 'region': 'Another Region'},
                          {'document': 'some another document', 'key': 'some_another_key', 'gender': 'male',
                           'age_from': 20, 'age_to': 25, 'region': 'Some Region'}])
    shard1 = SimpleSearchService(ds1)
    shard2 = SimpleSearchService(ds2)
    search_service = SearchInShardsService([shard1, shard2])
    res = search_service.get_search_data(
        'some another',
        user_data={'gender': 'female', 'age': '21'},
        geo_data={'region': 'Some Region'},
        limit=2
    )
    expected = pd.Series(['some_another_key', 'some_key'])
    assert np.array_equal(res.key, expected)
