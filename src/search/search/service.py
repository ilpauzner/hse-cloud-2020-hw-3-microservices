from typing import List

import pandas as pd
from pathos.multiprocessing import Pool

from common.base_search_service import BaseSearchService
from search_shard.service import SimpleSearchService


# noinspection PyMissingConstructor
# We construct self._data each time in get_search_data
class SearchInShardsService(SimpleSearchService):
    def __init__(self, shards: List[BaseSearchService]):
        self._shards = shards
        self._pool = Pool()

    def __del__(self):
        self._pool.close()

    def get_search_data(self, *args, **kwargs) -> pd.DataFrame:
        shards_responses = self._pool.map(lambda shard: shard.get_search_data(*args, **kwargs), self._shards)
        self._data = pd.concat(shards_responses)
        self._data.reset_index(inplace=True, drop=True)
        assert self._data.index.is_unique
        return super().get_search_data(*args, **kwargs)
