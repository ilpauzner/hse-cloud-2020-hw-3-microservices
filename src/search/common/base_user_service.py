from abc import abstractmethod


class BaseUserService:
    key = 'user_id'
    data_keys = ('gender', 'age')

    @abstractmethod
    def get_user_data(self, user_id):
        pass
