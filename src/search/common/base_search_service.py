from abc import abstractmethod

import pandas as pd


class BaseSearchService:
    DOCS_COLUMNS = ['document', 'key', 'key_md5']

    @abstractmethod
    def get_search_data(self, search_text, user_data=None, geo_data=None, limit=10) -> pd.DataFrame:
        pass
