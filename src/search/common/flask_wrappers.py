class FlaskClientWrapper:
    def __init__(self, client):
        self.client = client

    def get(self, *args, **kw):
        if 'params' in kw:
            kw['query_string'] = kw['params']
            del kw['params']
        return ResponseWrapper(self.client.get(*args, **kw))


class ResponseWrapper:
    def __init__(self, response):
        print(response)
        self.response = response

    def json(self):
        return self.response.json
