from abc import abstractmethod


class BaseGeoService:
    @abstractmethod
    def get_geo_data(self, ip_addr):
        pass
