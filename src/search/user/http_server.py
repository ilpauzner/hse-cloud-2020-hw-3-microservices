from flask import Flask, request

from user.service import UserService


class Server(Flask):
    def __init__(self, name: str, user_service: UserService):
        super().__init__(name)
        self._user_service = user_service
        urls = [
            ('/user_data', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        user_id = int(request.args.get('user_id'))
        user_data = self._user_service.get_user_data(user_id)
        return {'user_data': user_data}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
