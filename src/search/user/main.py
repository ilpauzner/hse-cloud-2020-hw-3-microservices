from common.data_source import CSV
from settings import USER_DATA_FILE
from user.http_server import Server
from user.service import UserService


def main():
    user_service = UserService(CSV(USER_DATA_FILE))
    server = Server('user', user_service=user_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
