import pytest

from common.data_source import AbstractDataSource
from common.flask_wrappers import FlaskClientWrapper
from user.http_client import UserServiceClient
from user.http_server import Server
from user.service import UserService


@pytest.fixture
def data_source():
    class SomeDataSource(AbstractDataSource):
        def read_data(self):
            return [{'user_id': 1, 'gender': 'male', 'age': 12},
                    {'user_id': 2, 'gender': 'female', 'age': 25}]

    return SomeDataSource()


@pytest.fixture
def user_service_impl(data_source):
    return UserService(data_source)


@pytest.fixture
def user_service_client(data_source):
    user_service = UserService(data_source)
    server = Server('user', user_service=user_service)
    client = FlaskClientWrapper(server.test_client())
    client.application = server
    return UserServiceClient(server_address="/", client=client)


def test_user_service(user_service_impl):
    assert user_service_impl.get_user_data(1) == {'gender': 'male', 'age': 12}
    assert user_service_impl.get_user_data(0) is None


def test_user_service_client(user_service_client):
    assert user_service_client.get_user_data(1) == {'gender': 'male', 'age': 12}
    assert user_service_client.get_user_data(0) is None
