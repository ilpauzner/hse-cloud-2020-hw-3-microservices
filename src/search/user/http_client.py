import urllib.parse

import requests

from common.base_user_service import BaseUserService


class UserServiceClient(BaseUserService):
    def __init__(self, server_address="http://user_service:8000", client=requests):
        self.url = urllib.parse.urljoin(server_address, 'user_data')
        self.client = client

    def get_user_data(self, user_id):
        response = self.client.get(self.url, params={'user_id': user_id})
        return response.json()['user_data']
